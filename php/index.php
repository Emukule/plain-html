<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.dropbtn {
  background-color: #3498DB;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.dropbtn:hover, .dropbtn:focus {
  background-color: #2980B9;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
</style>
</head>
<body>

<h2>Clickable Dropdown</h2>
<p>Click on the button to open the dropdown menu.</p>

<div class="dropdown">
  <button onclick="myFunction3()" class="dropbtn">Dropdown</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="1">1</a>
    <a href="2">2</a>
    <a href="3">3</a>
  </div>
</div>

<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function3(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
  
  <script>
function store_score(int) {
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
 
  xmlhttp.open("GET","store_score.php?score="+int,true);
  xmlhttp.send();
}
</script>

<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 5px;
}
th {
  text-align: left;
}
</style>



<h2>Cafe Menu</h2>


<table style="width:100%">
  <tr>
  <th>Item</th>
  <th>Unit Order</th> 
  <th>Price</th>
  <th>Item Total</th>
  </tr>
  <tr>
    <td>Strawberry Tarts</td>
    <td>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>

<div class="dropdown">
  <button onclick="myFunction3()" class="dropbtn">Dropdown</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="#">Link 1</a>
    <a href="#">Link 2</a>
    <a href="#">Link 3</a>
  </div>
</div>


    </td>
    <td>$4.5</td>
  </tr>
  <tr>
    <td>Beef Samosa</td>
    <td><input id="numbsamosa">

<button type="button" onclick="myFunctionsamosa()">Submit</button></td>
    <td>$6.0</td>

    <td><p id="sumsamosa"></p> </td>
  </tr>
  <tr>
    <td>Blueberry Muffin</td>
    <td><input id="numbmuffin" type="number" name="ticketNum" list="defaultNumbers">
<span class="validity"></span>

<datalist id="defaultNumbers">
  <option value="1">
  <option value="2">
  <option value="3">
  <option value="4">
  <option value="5">
</datalist>
    </td>

    <td>$2.0</td>

    <td><p id="summuffin"></p> </td>
</tr>

<script>
function myFunctionsamosa() {
  var x, text, sum;

  // Get the value of the input field with id="numb3"
  x = document.getElementById("numbsamosa").value;

 
  sum=x*6.0;
    text = "$ "+sum;
    
  document.getElementById("sumsamosa").innerHTML = text;
}
</script>

<script>
function myFunctionmuffin() {
  var x, text, sum;

  // Get the value of the input field with id="numb3"
  x = document.getElementById("numbmuffin").value;

 
  sum=x*2.0;
    text = "$ "+sum;
    
  document.getElementById("summuffin").innerHTML = text;
}
</script>


<script>
function myFunctionordertotal() {
  var x, text, sum;

  // Get the value of the input field with id="numb3"
  x = document.getElementById("numbsamosa").value
  y = document.getElementById("numbmuffin").value;

 
  sum=(x*6.0)+(y*2.0);
    text = "$ "+sum;
    
  document.getElementById("sumordertotal").innerHTML = text;
}
</script>
  
  <tr><td>total</td>
      <td></td>
      <td><input id="gettotal">

    <button type="button" onclick="myFunctionordertotal()">Calculate Total</button></td>
      <td id="sumordertotal"></td></tr>
      
</table>


<h2>JavaScript Math.random()</h2>

<p>Math.random() returns a random number between 0 (included) and 1 (excluded):</p>

<p id="demo"></p>

<script>
document.getElementById("demo").innerHTML = Math.round(100*Math.random());
</script>

<h2>JavaScript Variables</h2>

<p>In this example, x, y, and z are variables.</p>

<p id="demo2"></p>

<script>
var x = Math.round(100*Math.random());
var x = Math.round(100*Math.random());
var y = Math.round(100*Math.random());
var z = x + y;
document.getElementById("demo2").innerHTML =
"The value of x is: " + x + "The value of y is: " + y + "The value of x + y is : " + z;
</script>

<h2>JavaScript Can Validate Input</h2>

<p>Please input a number between 1 and 10 :</p>
<input id="numb">

<button type="button" onclick="myFunction()">Submit</button>

<p id="demo3"></p>

<script>
function myFunction() {
  var x, text;

  // Get the value of the input field with id="numb"
  x = document.getElementById("numb").value;

  // If x is Not a Number or less than one or greater than 10
  if (isNaN(x) || x < 1 || x > 10) {
    text = "Input not valid";
  } else {
    text = "Input OK";
  }
  document.getElementById("demo3").innerHTML = text;
}
</script>

  
<h2>JavaScript Quiz</h2>

<p>In this example, please add x + y before your time expires : </p>
<p id ="timer"></p>

<p id="demo4"></p>
  
<input id="numb2">

<button type="button" onclick="myFunction2()">Submit</button>
<p id="demo5"></p>

<script>
var truecount=0;
function myFunction2() {
  var ans, text;

  // Get the value of the input field with id="numb2"
  ans = document.getElementById("numb2").value;

  // If ans == z output correct
  if (isNaN(ans) || ans != z) {
  //  text = "False";
  } else {
   // text = "True";
   truecount++;
  }
  //document.getElementById("demo5").innerHTML = text;
}
</script>

<script>
// Set the date we're counting down to
var countDownDate = new Date().getTime() + 10*1000;
var questionnum=1;



var x = Math.round(100*Math.random());
var y = Math.round(100*Math.random());
var z = x + y;
document.getElementById("demo4").innerHTML =
"Question " + questionnum + "\n The value of x is: " + x + "The value of y is: " + y ; 


// Update the count down every 1 second
var xx = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("timer").innerHTML = seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    
    //clearInterval(xx);
    document.getElementById("timer").innerHTML = "Time Up!";
    countDownDate=new Date().getTime()+10*1000;
    x = Math.round(100*Math.random());
    y = Math.round(100*Math.random());
    z = x + y;
    questionnum++;
document.getElementById("demo4").innerHTML =
"Question " + questionnum + "\n The value of x is: " + x + "The value of y is: " + y ; 
    
  }
  if (questionnum>=5){
    store_score(truecount);
      document.getElementById("demo4").innerHTML="You got " + truecount + "correct out of 5";
      clearInterval(xx);
  }
}, 1000);

</script>

<h2>Radio Buttons</h2>

<p>Please select the correct answer of 3+7: </p>


  <button type="button" id="buttonA" onclick="myQuestionaireA()">10</button>
  <button type="button" id="buttonB" onclick="myQuestionaireB()">7</button>
  <button type="button" id="buttonC" onclick="myQuestionaireC()">13</button>



<p id="demo6">waiting for answer</p>

<script>
function myQuestionaireA() {
  
  document.getElementById("demo6").innerHTML = "You entered 10";
  document.getElementById("buttonA").style.backgroundColor="green";
  document.getElementById("buttonB").style.backgroundColor="transparent";
  document.getElementById("buttonC").style.backgroundColor="transparent";
}
</script>
<script>
function myQuestionaireB() {
  
  document.getElementById("demo6").innerHTML = "You entered 7";
  document.getElementById("buttonA").style.backgroundColor="transparent";
  document.getElementById("buttonB").style.backgroundColor="green";
  document.getElementById("buttonC").style.backgroundColor="transparent";
}
</script>
 <script>
function myQuestionaireC() {
  
  document.getElementById("demo6").innerHTML = "You entered 13";
  document.getElementById("buttonA").style.backgroundColor="transparent";
  document.getElementById("buttonB").style.backgroundColor="transparent";
  document.getElementById("buttonC").style.backgroundColor="green";
}
</script>
  <?php
$myfile = fopen("newfile.txt", "a") or die("Unable to open file!");
$txt = "Mickey Mouse\n";
fwrite($myfile, $txt);
$txt = "Minnie Mouse\n";
fwrite($myfile, $txt);
fclose($myfile);
?>
  
<?php
$myfile = fopen("newfile.txt", "r") or die("Unable to open file!");
echo fread($myfile,filesize("newfile.txt"));
fclose($myfile);
?>
  
  <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    
    


Result Size: 497 x 590
<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
  }
    
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

Name: <input type="text" name="name">
<span class="error">* <?php echo $nameErr;?></span>
<br><br>
E-mail:
<input type="text" name="email">
<span class="error">* <?php echo $emailErr;?></span>
<br><br>
Website:
<input type="text" name="website">
<span class="error"><?php echo $websiteErr;?></span>
<br><br>
Comment: <textarea name="comment" rows="5" cols="40"></textarea>
<br><br>
Gender:
<input type="radio" name="gender" value="female">Female
<input type="radio" name="gender" value="male">Male
<input type="radio" name="gender" value="other">Other
<span class="error">* <?php echo $genderErr;?></span>
<br><br>
<input type="submit" name="submit" value="Submit"> 

</form>
  
<?php
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
echo "<br>";
echo $comment;
echo "<br>";
echo $gender;
?>
  
<script>
function getVote(int) {
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("poll").innerHTML=this.responseText;
    }
  }
  xmlhttp.open("GET","poll_vote.php?vote="+int,true);
  xmlhttp.send();
}
</script>

<div id="poll">
<h3>Do you like PHP and AJAX so far?</h3>
<form>
Yes:
<input type="radio" name="vote" value="0" onclick="getVote(this.value)">
<br>No:
<input type="radio" name="vote" value="1" onclick="getVote(this.value)">
</form>
</div>
  
</body>
</html> 